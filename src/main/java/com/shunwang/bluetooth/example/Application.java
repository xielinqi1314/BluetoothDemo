package com.shunwang.bluetooth.example;

import com.shunwang.bluetooth.BluetoothService;

public class Application {
	
    /**
     * 
     * @param args
     */
	public static void main(String[] args) {
		BluetoothService bluetoothService = new BluetoothService();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		bluetoothService.stop();//一直没有设备接入的情况未做处理
	}
	
}
