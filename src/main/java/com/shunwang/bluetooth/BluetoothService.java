package com.shunwang.bluetooth;

import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.UUID;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.microedition.io.StreamConnectionNotifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.LoggerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BluetoothService implements Runnable {
    private Boolean                      stopFlag         = false;
    // 本机蓝牙设备
    private LocalDevice                  local            = null;

    // 流连接
    private StreamConnection             streamConnection = null;
    // 接受数据的字节流
    private byte[]                       acceptdByteArray = new byte[1024];
    // 输入流
    private DataInputStream              inputStream;
//    // 接入通知
//    private StreamConnectionNotifier     notifier;

    private final static ExecutorService service          = Executors.newCachedThreadPool();

    private final static Logger logger = LoggerFactory.getLogger(BluetoothService.class);
    static {
        LoggerContext loggerContext = (LoggerContext) LogManager.getContext(false);
        File file = new File("log4j2.xml");
        loggerContext.setConfigLocation(file.toURI());
    }
    
    public BluetoothService() {
        logger.debug("=======================");
        try {
            // 这两步不一定要
            BluCatUtil.doctorDevice(); // 驱动检查
            RemoteDeviceDiscovery.runDiscovery(); // 搜索附近所有的蓝牙设备
            // System.out.println(RemoteDeviceDiscovery.getDevices());
        } catch (IOException | InterruptedException e1) {
            e1.printStackTrace();
        }
        try {
            local = LocalDevice.getLocalDevice();

            if (!local.setDiscoverable(DiscoveryAgent.GIAC))
                System.out.println("请将蓝牙设置为可被发现");

            Set<RemoteDevice> devicesDiscovered = RemoteDeviceDiscovery.getDevices(); // 附近所有的蓝牙设备，必须先执行 runDiscovery 连接
            if (devicesDiscovered.iterator().hasNext()) {
                RemoteDevice first = devicesDiscovered.iterator().next();
                logger.debug("连接蓝牙地址===" + first.getBluetoothAddress());
                streamConnection = (StreamConnection) Connector.open("btspp://" + first.getBluetoothAddress() + ":1");
            }
            
//            /**
//             * 作为服务端，被请求
//             */
//            String url = "btspp://localhost:" + new UUID(80087355).toString() + ";name=RemoteBluetooth";
//            notifier = (StreamConnectionNotifier) Connector.open(url);
        } catch (Exception e) {
            e.printStackTrace();
        }
        service.submit(this);
    }

    @Override
    public void run() {
        try {
//            String inStr = null;
//            streamConnection = notifier.acceptAndOpen(); // 阻塞的，等待设备连接，这里就没有作中断处理了，如果没有连接该线程就无法关闭
//            inputStream = streamConnection.openDataInputStream();
//            int length;
//            while (true) {
//                if ((inputStream.available()) <= 0) { // 不阻塞线程
//                    if (stopFlag) // UI停止后，关闭
//                        break;
//                    Thread.sleep(800); // 数据间隔比较长，手动堵塞线程
//                } else {
//                    length = inputStream.read(acceptdByteArray);
//                    if (length > 0) {
//                        inStr = new String(acceptdByteArray, 0, length);
//                        System.out.println(inStr);
//                    }
//                }
//            }
            
            String inSTR = null;
            // 获取流通道
            if(streamConnection != null) {
                inputStream = streamConnection.openDataInputStream();
                if(inputStream != null) {
                  // 读取字节流
                  while (inputStream.read(acceptdByteArray) != -1) {
                      inSTR = new String(acceptdByteArray);
                      System.out.println(inSTR);
                      if (stopFlag 
                              || inSTR.contains("EXIT")) {
                          break;
                      }
                  }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null)
                    inputStream.close();
                if (streamConnection != null)
                    streamConnection.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized void stop() {
        stopFlag = true;
        service.shutdown();
    }
}
